/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;
import java.text.DecimalFormat;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class TestPerson {
    public static void main(String[] args) {
    Person personOne = new Person("James", 82, WeightUnit.KILOGRAMS, 172, HeightUnit.CM);
        Person personTwo = new Person("David", 152, WeightUnit.POUND, 70, HeightUnit.INCHES);
        
        System.out.println("BMI of Person one: " + new DecimalFormat("##.##").format(personOne.getBmi()));
        System.out.println("BMI of Person Two: " + new DecimalFormat("##.##").format(personTwo.getBmi()));
}
}
