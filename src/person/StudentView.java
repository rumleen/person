/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;
import java.text.DecimalFormat;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class StudentView {
     Student model;

    public StudentView(Student model) {
        this.model = model;
    }

    public void print() {
        System.out.println("\nStudent View: Printing student data from View \n");

        System.out.println("Student View: Student ID: " + model.getId());
        System.out.println("Student View: Name: " + model.getName());
        System.out.println("Student View: Weight: " + model.getWeight() + " " + (model.getWeightUnit() == WeightUnit.KILOGRAMS ? "kgs" : "lbs"));
        System.out.println("Student View: Height: " + (model.getHeightUnit() == HeightUnit.INCHES ? (new DecimalFormat("00").format((int) model.getHeight() / 12)) + "\'" + new DecimalFormat("#").format(model.getHeight() % 12) + "\"" : model.getHeight() + " cm"));
        System.out.println("Student View: BMI: " + new DecimalFormat("##.##").format(model.getBmi()));


    }
}
