/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;
import java.text.DecimalFormat;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class TestStudent {
    public static void main(String[] args) {


        // Create student model
        Student studentModel = fetchStudentData();

        //Create student view and initialize it by providing student model.
        StudentView studentView = new StudentView(studentModel);
        // Create Student controller and bind it with model and view.

        StudentController studentController = new StudentController(studentModel, studentView);

        // Print student data
        studentController.updateView();

        // Makes changes in the data through controller
        studentController.setName("James");
        studentController.setId(02);
        studentController.setHeightUnit(HeightUnit.CM);
        studentController.setHeight(172);
        studentController.setWeightUnit(WeightUnit.KILOGRAMS);
        studentController.setWeight(82);

        // Print the data again.
        studentController.updateView();
    }

    // fetch dummy data and return student object.
    public static Student fetchStudentData() {

        Student s = new Student(01, "Johnson", 152, WeightUnit.POUND ,70, HeightUnit.INCHES);
        return s;


    }
}
