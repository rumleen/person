/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Student extends Person {
    private int id;

    public Student(int id, String name, double weight, WeightUnit weightUnit, double height, HeightUnit heightUnit) {
        super(name, weight, weightUnit, height, heightUnit);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
