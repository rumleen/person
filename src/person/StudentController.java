/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class StudentController {
     private Student model;
    private StudentView view;

    public StudentController(Student _model, StudentView _view) {
        model = _model;
        this.view = _view;
    }

    public void updateView() {

        System.out.println("StudentController: Printing model data through controller by calling view.print()");
        view.print();
        System.out.println();
    }


    public void setId(int id) {
        model.setId(id);
        System.out.println("StudentController: Student ID updated successfully...");

    }

    public void setName(String name) {

        model.setName(name);
        System.out.println("StudentController: Student Name updated successfully...");

    }

    public void setHeight(int height) {
        model.setHeight(height);
        System.out.println("StudentController: Student Height updated successfully...");

    }

    public void setWeight(double weight) {
        model.setWeight(weight);
        System.out.println("StudentController: Student Weight updated successfully...");

    }

    public void setWeightUnit(WeightUnit weightUnit){
        model.setWeightUnit(weightUnit);
        System.out.println("StudentController: Student Weight Unit updated successfully...");

    }

    public void setHeightUnit(HeightUnit heightUnit){
        model.setHeightUnit(heightUnit);
        System.out.println("StudentController: Student Height unit updated successfully...");
    }
}
