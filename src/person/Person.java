/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package person;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Person {

    private String name;
    private double weight;
    private double height;
    private HeightUnit heightUnit;
    private WeightUnit weightUnit;
    private double bmi;

    public HeightUnit getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(HeightUnit heightUnit) {
        this.heightUnit = heightUnit;
        setBmi();
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(WeightUnit weightUnit) {
        this.weightUnit = weightUnit;
        setBmi();
    }

    public double getBmi() {
        return bmi;
    }

    public Person(String name, double weight, WeightUnit weightUnit, double height, HeightUnit heightUnit) {

        this.heightUnit = heightUnit;
        this.weightUnit = weightUnit;
        this.name = name;
        this.weight = weight;
        this.height = height;


        setBmi();


    }

    private void setBmi() {
        switch (weightUnit) {
            case KILOGRAMS: {
                switch (heightUnit) {
                    case CM: {
                        double meters = cmToMeter(height);
                        bmi = weight / (meters * meters);
                        break;
                    }
                    case INCHES: {
                        double meters = inchesToMeters(height);
                        bmi = weight / (meters * meters);
                        break;
                    }
                }
                break;
            }
            case POUND: {
                switch (heightUnit) {
                    case CM: {
                        double heightInMeters = cmToMeter(height);
                        bmi = poundsToKG(weight) / (heightInMeters * heightInMeters);
                        break;
                    }
                    case INCHES: {
                        double meters = inchesToMeters(height);
                        bmi = poundsToKG(weight) / (meters * meters);
                        break;
                    }
                }
                break;
            }
        }

    }


    private double poundsToKG(double pounds) {
        double kg = pounds * 0.454;
        return kg;
    }

    private double cmToMeter(double cm) {
        return cm * 0.01;
    }

    private double inchesToMeters(double inches) {
        return inches * 0.0254;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {


        this.weight = weight;
        setBmi();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {


        this.height = height;
        setBmi();
    }
    
}

enum HeightUnit {
    INCHES,
    CM;
}

enum WeightUnit {
    KILOGRAMS,
    POUND
}